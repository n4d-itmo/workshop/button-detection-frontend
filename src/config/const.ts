export const BACKEND_API_URL =
  process.env.NEXT_PUBLIC_BACKEND_API_URL ?? "http://localhost:8000/";

export const BUNDLER_URL = process.env.NEXT_PUBLIC_BUNDLER_URL;

export enum Tokens {
  TEXT = "text",
  COLOR = "color",
}

export enum Colors {
  // RED = "#4CAF50",
  BLUE = "#008CBA",
  // GREEN = "#4CAF50",
  PINK = "#FF1493",
  ORANGE = "#FFA500",
  GOLD = "#FFD700",
  PURPLE = "#800080",
}
