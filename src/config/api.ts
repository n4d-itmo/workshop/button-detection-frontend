"use-client";

import axios from "axios";

import { BACKEND_API_URL } from "./const";

const baseURL = (() => {
  if (typeof window !== "undefined") {
    try {
      const url = new URL(BACKEND_API_URL);
      return url.toString();
    } catch (e) {
      return new URL(
        BACKEND_API_URL,
        window?.location.origin ?? "https://interbox.space/"
      ).toString();
    }
  }
  return BACKEND_API_URL;
})();

const api = axios.create({ baseURL });

const AUTH_TOKEN = "authToken";

const logout = () => {
  const { Authorization, ...rest } = api.defaults.headers;
  api.defaults.headers = rest;
  localStorage?.removeItem(AUTH_TOKEN);
};

const login = (token: string) => {
  if (token) {
    api.defaults.headers["Authorization"] = `Bearer ${token}`;
    localStorage?.setItem(AUTH_TOKEN, token);
  }
};

// const t = localStorage?.getItem(AUTH_TOKEN);
// if (t) login(t);

export { api, login, logout };
