import { useMutation, useQuery, useQueryClient } from "react-query";
import axios from "axios";
import { BACKEND_API_URL } from "@/config/const";

interface Vid {
  id: string;
  title: string;
  uploaded_at: string;
  status: string;
}

interface VidExtended extends Vid {
  video_url: string;
  frames: {
    image_path: string;
    label: string[];
    second: number;
  }[];
  processed_video: string;
  status: "NOT_STARTED" | "PROCESS" | "DONE";
}

const getVideos = async () => {
  const { data } = await axios.get<Vid[]>(`${BACKEND_API_URL}video/list/`, {
    withCredentials: false,
  });
  return data;
};

const getVideo = async (id: string) => {
  const {
    data: { processed_video, frames, ...rest },
  } = await axios.get<VidExtended>(`${BACKEND_API_URL}video/${id}/`, {
    withCredentials: false,
  });
  const data = {
    ...rest,
    processed_video: processed_video
      ? new URL("/download" + processed_video, BACKEND_API_URL).toString()
      : null,
    frames: frames
      ? (JSON.parse(frames as unknown as string) as VidExtended["frames"]).map(
          (v) => {
            const label = (v?.label as unknown as string);
            const splitted = label.split(',');
            const mapped = splitted.map(v => v.match(/\d/g)?.join(''));

            return {
              image_path: new URL(
                "/download" + v.image_path,
                BACKEND_API_URL
              ).toString(),
              label: mapped,
              second: v.second,
            };
          }
        )
      : null,
  };
  return data;
};

const deleteVideo = async (id: string) => {
  const { data } = await axios.delete<VidExtended>(
    `${BACKEND_API_URL}video/${id}/`,
    {
      withCredentials: false,
    }
  );
  return data;
};

const processVid = async (id: string) => {
  const { data } = await axios.post<VidExtended>(
    `${BACKEND_API_URL}video/process/${id}`,
    null,
    {
      withCredentials: false,
    }
  );
  return data;
};

const uploadVid = async (f: File) => {
  const { data } = await axios.post(`${BACKEND_API_URL}video/upload/`, f, {
    withCredentials: false,
  });
  return data;
};

const useVids = () =>
  useQuery({
    queryKey: ["vids"],
    queryFn: getVideos,
    enabled: false
  });

const useUploadVid = () => {
  const { mutate: process } = useProcessVid();

  const qc = useQueryClient();

  return useMutation({
    mutationFn: uploadVid,
    onSuccess: (d) => {
      qc.invalidateQueries("vids");
      process(d.video_id)
    },
  });
};

const useDeleteVid = ({ onSuccess }: { onSuccess?: () => {} } = {}) => {
  const qc = useQueryClient();

  return useMutation({
    mutationFn: deleteVideo,
    onSuccess: () => {
      qc.invalidateQueries("vids");
      qc.invalidateQueries("vid");
      onSuccess?.();
    },
  });
};

const useProcessVid = ({ onSuccess }: { onSuccess?: () => {} } = {}) => {
  const qc = useQueryClient();

  return useMutation({
    mutationFn: processVid,
    onSuccess: () => {
      qc.invalidateQueries("vids");
      qc.invalidateQueries("vid");
      onSuccess?.();
    },
  });
};

const useVid = (id: string) => {
  const { mutate: process } = useProcessVid();
  return useQuery({
    queryKey: ["vid", id],
    queryFn: () => getVideo(id),
    retry: false,
    onSuccess: (d) => {
      if (d.status === "NOT_STARTED") {
        process(id);
      }
    },
  });
};

export { useVids, useUploadVid, useVid, useDeleteVid };
