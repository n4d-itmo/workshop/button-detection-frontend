import { useMutation, useQuery, useQueryClient } from "react-query";

import { api } from "@/config/api";

interface ProjectDto {
  id: number;
  title: string;
  description: string;
  owner_id: number;
}

interface ElementDto {
  id: number;
  name: string;
  // stringified json
  blob_data: string;
  project_id: number;
  // base64 encoded
  picture: string;
}

export interface TokenDto {
  color: string;
  "font-family": string;
  "font-style": string;
  "text-align": string;
  "text-transform": string;
  "background-color": string;
  "border-color": string;
  "border-style": string;
}

export interface TokenEntity {
  color: string;
  bgc: string;
  bdrc: string;
  bdrs: string;
  ff: string;
  fs: string;
  txa: string;
  txtf: string;
}

const defaultTokens: TokenDto & { isNoDetection?: boolean } = {
  color: "#FFD700",
  "font-family": "Arial",
  "font-style": "normal",
  "text-align": "justify",
  "text-transform": "none",
  "background-color": "#008CBA",
  "border-color": "#FFD700",
  "border-style": "double",
  isNoDetection: true,
};

const findElementsByProject = async (projectId: string | number) => {
  const { data } = await api.get<ElementDto[]>(
    `/projects/${projectId}/elements/`
  );
  const { blob_data, ...rest } = data[0] ?? { id: 0, name: "DELETE ME" };

  const _tokens: TokenDto & { isNoDetection?: boolean } = (() => {
    try {
      return JSON.parse(blob_data)[0] ?? defaultTokens;
    } catch (e) {
      console.log(e);
      return defaultTokens;
    }
  })();

  const tokens: TokenEntity = {
    color: _tokens.color,
    bgc: _tokens["background-color"],
    bdrc: _tokens["border-color"],
    bdrs: _tokens["border-style"],
    ff: _tokens["font-family"],
    fs: _tokens["font-style"],
    txa: _tokens["text-align"],
    txtf: _tokens["text-transform"],
  };

  return { ...rest, tokens, css: _tokens, isNoDetection: _tokens.isNoDetection };
};

const findProjects = async () => {
  const { data } = await api.get<ProjectDto[]>(`/projects/`);

  const projects = await Promise.all(
    data.map(async (v) => {
      const element = await findElementsByProject(v.id);

      return { ...v, element };
    })
  );

  return projects;
};

const createProject = async (d: { title: string; description: string }) => {
  const { data: project } = await api.post<ProjectDto>(`/projects/`, d);

  return project;
};

const createElementInProject = async ({
  projectId,
  file,
}: {
  projectId: string | number;
  file: FormData;
}) => {
  const { data: element } = await api.post<ElementDto>(
    `/projects/${projectId}/elements/`,
    file
  );

  return element;
};

const uploadScreenshot = async (d: { f: FormData; name: string }) => {
  const project = await createProject({ title: d.name, description: d.name });
  const element = await createElementInProject({
    projectId: project.id,
    file: d.f,
  });
  return { ...project, element };
};

const deleteProject = async (projectId: string | number) => {
  const { data: project } = await api.delete<ProjectDto>(
    `/projects/${projectId}/`
  );

  return project;
};

const useProjects = () =>
  useQuery({
    queryKey: ["projects"],
    queryFn: findProjects,
  });

const useUploadScreenshot = () => {
  const qc = useQueryClient();

  return useMutation({
    mutationFn: uploadScreenshot,
    onSuccess: (d) => {
      qc.invalidateQueries("projects");
    },
  });
};

const useProject = (projectId: string | number | undefined) => {
  const { data, ...rest } = useProjects();
  return {
    ...rest,
    data: data?.find((p) => p.id.toString() === projectId?.toString()),
  };
};

const useDeleteProject = ({ onSuccess }: { onSuccess?: () => {} } = {}) => {
  const qc = useQueryClient();

  return useMutation({
    mutationFn: deleteProject,
    onSuccess: () => {
      qc.invalidateQueries("projects");
      onSuccess?.();
    },
  });
};

export { useProject, useProjects, useUploadScreenshot, useDeleteProject };
