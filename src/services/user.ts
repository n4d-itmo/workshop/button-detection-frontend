import { useQuery } from "react-query";

import { api, login } from "@/config/api";

interface ProfileDto {
  id: number;
  username: string;
  email: string;
}

const getProfile = async () => {
  const response = await api.get<ProfileDto>(`/users/me/`);
  return response.data;
};

const useProfile = () =>
  useQuery({
    queryKey: ["profile"],
    queryFn: getProfile,
  });

export { useProfile };
