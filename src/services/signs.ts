import { useQuery } from "react-query";

interface Sign {
  id: string;
  name: string;
  description: string;
}

const useSigns = () =>
  useQuery({
    queryKey: ["signs"],
    queryFn: () =>
      new Promise<Sign[]>((res) => {
        setTimeout(() => {
          res([
            {
              id: "1",
              name: "Straight Road",
              description:
                "A stop sign is a traffic sign designed to notify drivers that they must come to a complete stop and make sure the intersection is safely clear of vehicles and ",
            },
            
            {
              id: "2",
              name: "Not a straight road",
              description:
                "A stop sign is a traffic sign designed to notify drivers that they must come to a complete stop and make sure the intersection is safely clear of vehicles and ",
            },
            
            // {
            //   id: "3",
            //   name: "Semi-Straight Road",
            //   src: signSrc,
            //   description:
            //     "A stop sign is a traffic sign designed to notify drivers that they must come to a complete stop and make sure the intersection is safely clear of vehicles and ",
            // },
            
          ]);
        });
      }),
    cacheTime: 500,
  });

// const useChat = ({ chatId }: { chatId: number }) => {
//   const { user, accessToken } = useAuthInfo();

//   return useQuery({
//     queryKey: ["chat", { chatId, userId: user?.userId }],
//     queryFn: (v) => {
//       const chatId =
//         (typeof v.queryKey[1] === "object" && v.queryKey[1].chatId) || 0;
//       return getChat({ token: accessToken ?? "", chatId });
//     },
//   });
// };

// const useCreateChat = ({
//   onSuccess,
// }: {
//   onSuccess?: (chatId: number) => void;
// }) => {
//   const client = useQueryClient();
//   const { accessToken } = useAuthInfo();
//   return useMutation({
//     mutationFn: () => createChat({ token: accessToken ?? "" }),
//     onSuccess: (d) => {
//       onSuccess?.(d.id);
//       client.refetchQueries(["chats"]);
//     },
//   });
// };

// const useUpdateChat = () => {
//   const client = useQueryClient();
//   const { accessToken } = useAuthInfo();
//   return useMutation({
//     mutationFn: ({ chatId, name }: { chatId: number; name: string }) =>
//       updateChat({ token: accessToken ?? "", chatId, name }),
//     onSuccess: (d) => {
//       client.refetchQueries(["chats"]);
//       client.refetchQueries(["chat", { chatId: d.id }]);
//     },
//   });
// };

// const useDeleteChat = ({
//   onSuccess,
// }: {
//   onSuccess?: (chatId: number) => void;
// }) => {
//   const client = useQueryClient();
//   const { accessToken } = useAuthInfo();
//   return useMutation({
//     mutationFn: ({ chatId }: { chatId: number }) =>
//       deleteChat({ chatId, token: accessToken ?? "" }),
//     onSuccess: (d) => {
//       onSuccess?.(d.id);
//       client.removeQueries(["messages", d.id]);
//       client.refetchQueries(["chats"]);
//     },
//   });
// };

export { useSigns };
