import { useQuery } from "react-query";

import { api, login } from "@/config/api";

const auth = async () => {
  const params = new URLSearchParams();
  params.append("username", "user");
  params.append("password", "user");

  const { data } = await api.post<{ access_token: string }>(`token/`, params);

  login(data.access_token);

  return data;
};

const useAuth = () =>
  useQuery({
    queryKey: ["auth"],
    queryFn: auth,
    retry: false,
    cacheTime: 1000 * 60 * 10,
    refetchOnWindowFocus: false,
  });

export { useAuth };
