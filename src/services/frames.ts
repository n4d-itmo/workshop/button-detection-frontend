import { useQuery } from "react-query";

interface TimelineFrame {
  id: string;
  name: string;
  description: string;
}

const useFrames = () =>
  useQuery({
    queryKey: ["frames"],
    queryFn: () =>
      new Promise<TimelineFrame[]>((res) => {
        setTimeout(() => {
          res([
            { id: "1", name: "Frame #1", description: "A stop sign is a traffic sign designed" },
            { id: "2", name: "Frame #2", description: "A stop sign is a traffic sign designed" },
            { id: "3", name: "Frame #3", description: "A stop sign is a traffic sign designed" },
            { id: "4", name: "Frame #4", description: "A stop sign is a traffic sign designed" },
            { id: "5", name: "Frame #5", description: "A stop sign is a traffic sign designed" },
            { id: "6", name: "Frame #6", description: "A stop sign is a traffic sign designed" },
          ]);
        });
      }),
    cacheTime: 500,
  });

// const useChat = ({ chatId }: { chatId: number }) => {
//   const { user, accessToken } = useAuthInfo();

//   return useQuery({
//     queryKey: ["chat", { chatId, userId: user?.userId }],
//     queryFn: (v) => {
//       const chatId =
//         (typeof v.queryKey[1] === "object" && v.queryKey[1].chatId) || 0;
//       return getChat({ token: accessToken ?? "", chatId });
//     },
//   });
// };

// const useCreateChat = ({
//   onSuccess,
// }: {
//   onSuccess?: (chatId: number) => void;
// }) => {
//   const client = useQueryClient();
//   const { accessToken } = useAuthInfo();
//   return useMutation({
//     mutationFn: () => createChat({ token: accessToken ?? "" }),
//     onSuccess: (d) => {
//       onSuccess?.(d.id);
//       client.refetchQueries(["chats"]);
//     },
//   });
// };

// const useUpdateChat = () => {
//   const client = useQueryClient();
//   const { accessToken } = useAuthInfo();
//   return useMutation({
//     mutationFn: ({ chatId, name }: { chatId: number; name: string }) =>
//       updateChat({ token: accessToken ?? "", chatId, name }),
//     onSuccess: (d) => {
//       client.refetchQueries(["chats"]);
//       client.refetchQueries(["chat", { chatId: d.id }]);
//     },
//   });
// };

// const useDeleteChat = ({
//   onSuccess,
// }: {
//   onSuccess?: (chatId: number) => void;
// }) => {
//   const client = useQueryClient();
//   const { accessToken } = useAuthInfo();
//   return useMutation({
//     mutationFn: ({ chatId }: { chatId: number }) =>
//       deleteChat({ chatId, token: accessToken ?? "" }),
//     onSuccess: (d) => {
//       onSuccess?.(d.id);
//       client.removeQueries(["messages", d.id]);
//       client.refetchQueries(["chats"]);
//     },
//   });
// };

export { useFrames };
