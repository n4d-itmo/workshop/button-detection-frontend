"use client";

import { useEffect, useMemo, useRef } from "react";
import { useRouter } from "next/navigation";
import s from "../vids.module.css";
import Timeline from "../components/Timeline";
import sLocal from "./vid.module.css";
import { FormProvider, useForm } from "react-hook-form";
import { Tokens } from "@/config/const";
import {
  SandpackProvider,
  SandpackLayout,
  SandpackCodeEditor,
  SandpackPreview,
} from "@codesandbox/sandpack-react";

import * as themes from "@codesandbox/sandpack-themes";
import { TokenEntity, useProject } from "@/services/projects";
import clsx from "clsx";
import tinycolor from "tinycolor2";

// atom dark
// cyberpunk
// dracula
// const theme = themes.dracula;

const app = `import Button from './Button.js';

const styles = {
  margin: "auto",
  width: "100wh",
  height: "100vh",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
};

export default function() {
  return (
    <div style={styles}>
      <Button />
    </div>
  );
}`;

const button = (
  tokens: Partial<TokenEntity> & { text: string }
) => `import Button from '@mui/material/Button';
import { styled } from '@mui/material/styles';

const BootstrapButton = styled(Button)({
  color: "${tokens.color}",
  backgroundColor: "${tokens.bgc}",
  ":hover": {
    backgroundColor: "#${tinycolor(tokens.bgc)
      .lighten(15)
      .saturate(15)
      .toHex()}",
  },
});

export default function ButtonRenderer() {
  return (
    <BootstrapButton
      variant="contained"
      size="sm"
      disableElevation
    >
      ${tokens.text}
    </BootstrapButton>
  );
}`;

const defaultTokens = {
  [Tokens.TEXT]: "Sign Up",
  // color: { id: Colors.BLUE, name: Colors.BLUE },
  // bgc: { id: Colors.BLUE, name: Colors.BLUE },
};

export default function VidPage({ params }: { params: { id: string } }) {
  const { replace } = useRouter();
  const { data: project } = useProject(params.id);

  const methods = useForm({ defaultValues: defaultTokens });
  const { watch } = methods;
  // @ts-expect-error
  const tokens = watch([Tokens.TEXT, "color", "bgc"]);

  useEffect(() => {
    const tks = project?.element.tokens ?? {};
    Object.keys(tks).forEach((k) => {
      if (["color", "bgc"].includes(k)) {
        // @ts-expect-error
        methods.setValue(k, { id: tks[k], name: tks[k] });
      } else {
        // @ts-expect-error
        methods.setValue(k, tks[k]);
      }
    });
  }, [project?.element.tokens, methods.setValue]);

  const templated = useMemo(() => {
    return button({
      text: tokens[0],
      ...(project?.element.tokens ?? {}),
      // @ts-expect-error
      color: (tokens[1] && tokens[1].id) ?? project?.element.tokens.color,
      // @ts-expect-error
      bgc: (tokens[2] && tokens[2].id) ?? project?.element.tokens.bgc,
    });
    // @ts-expect-error
  }, [project?.element.tokens, tokens[0], tokens[1] && tokens[1].id, tokens[2] && tokens[2].id]);

  const shouldRenderSandpack =
    project?.element.tokens && !project?.element.isNoDetection;

  return (
    <>
      <div className={s.workspace}>
        <div className={clsx(s.header, "flex", "flex-col")}>
          <p>
            Awesomely{" "}
            <span
              className={clsx({
                ["line-through"]: !shouldRenderSandpack,
              })}
            >
              Parsed
            </span>{" "}
            Button #{project?.id}
          </p>
          <p className="font-light text-sm text-slate-500">
            {project?.element.name ?? "UFO kidnapped button name"}
          </p>
        </div>
        <div className={s.playerContainer}>
          {shouldRenderSandpack ? (
            <SandpackProvider
              template="react"
              // theme={theme}
              customSetup={{
                dependencies: {
                  "@mui/material": "~5.15",
                  "@emotion/react": "~11.11",
                  "@emotion/styled": "~11.11",
                },
              }}
              files={{
                "App.js": {
                  code: app,
                  hidden: true,
                },
                "Button.js": {
                  active: true,
                  code: templated,
                  readOnly: true,
                },
              }}
            >
              <SandpackLayout>
                <SandpackPreview />
                <SandpackCodeEditor />
              </SandpackLayout>
            </SandpackProvider>
          ) : (
            <p className="p-10 mt-10 text-center">No Detections Found 🤷‍♂️</p>
          )}
        </div>
        <div className={sLocal.signsList}></div>
      </div>

      <div className={s.timeline}>
        <FormProvider {...methods}>
          <form onSubmit={methods.handleSubmit(console.log)}>
            <Timeline projectId={project?.id.toString()} />
          </form>
        </FormProvider>
      </div>
    </>
  );
}
