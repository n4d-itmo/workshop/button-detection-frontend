"use client";

import React, { useRef } from "react";
import { useAuth } from "@/services/auth";
import s from "../vids.module.css";

import Button from "@mui/material/Button";
import { styled } from "@mui/material/styles";
import { TokenEntity } from "@/services/projects";
import tinycolor from "tinycolor2";

enum Colors {
  // RED = "#4CAF50",
  BLUE = "#008CBA",
  // GREEN = "#4CAF50",
  PINK = "#FF1493",
  ORANGE = "#FFA500",
  GOLD = "#FFD700",
  PURPLE = "#800080",
}

const buttons: Partial<TokenEntity>[] = [
  {
    color: Colors.PURPLE,
    bgc: Colors.GOLD,
  },
  {
    color: Colors.PINK,
    bgc: Colors.BLUE,
  },
  {
    color: Colors.GOLD,
    bgc: Colors.PINK,
  },
  {
    color: Colors.GOLD,
    bgc: Colors.PURPLE,
  },
  {
    color: Colors.PURPLE,
    bgc: Colors.ORANGE,
  },
  {
    color: Colors.PURPLE,
    bgc: Colors.GOLD,
  },
];

const styledButton = (tokens: Partial<TokenEntity>) =>
  styled(Button)({
    color: tokens.color,
    backgroundColor: tokens.bgc,
  });

function Home() {
  const { data: d, error } = useAuth();

  const C = styled(Button)({
    color: Colors.PURPLE,
    backgroundColor: Colors.GOLD,
    borderColor: Colors.PURPLE,
    ":hover": {
      backgroundColor: `#${tinycolor(Colors.GOLD)
        .lighten(15)
        .saturate(15)
        .toHex()}`,
    },
    borderWidth: 1,
    borderStyle: "solid",
  });

  if (!d || error) {
    return null;
  }

  return (
    <div>
      <p className={s.header}>Detectable Buttons Showroom</p>

      <div className="p-6 grid grid-cols-3 gap-10">
        {buttons.map((tokens, i) => {
          const Button = styledButton(tokens);
          return (
            <div
              key={i}
              className="bg-gray-100 p-10 rounded-lg flex items-center justify-center"
            >
              <Button className="pointer-events-none">Button #{i + 1}</Button>
            </div>
          );
        })}
      </div>

      {/* <div className="bg-gray-100 p-10 rounded-lg flex items-center justify-center max-w-[150px] mx-auto">
        <C>Button</C>
      </div> */}
    </div>
  );
}

export default Home;
