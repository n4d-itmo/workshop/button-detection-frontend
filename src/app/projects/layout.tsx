"use client";

import React from "react";
import Head from "next/head";
import { usePathname } from "next/navigation";
import { useAuth } from "@/services/auth";
import cn from "clsx";
import Menu from "./components/Menu";
import s from "./vids.module.css";

export default function Home({ children }: { children: any }) {
  const { data: d, error } = useAuth();
  const pathname = usePathname();
  const isCreationScreen = ["/projects", "/projects/showroom"].includes(
    pathname
  );

  if (!d || error) {
    return null;
  }

  return (
    <div>
      <Head>
        <title>Button Detection App</title>
      </Head>

      <div
        className={cn(isCreationScreen ? s.layout_without_timeline : s.layout)}
      >
        <div className={s.sidebar}>
          <Menu />
        </div>

        {children}
      </div>
    </div>
  );
}
