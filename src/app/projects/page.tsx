"use client";

import React, { useRef } from "react";
import { useAuth } from "@/services/auth";
import s from "./vids.module.css";
import { useUploadScreenshot } from "@/services/projects";
import { useRouter } from "next/navigation";

function Home() {
  const { data: d, error } = useAuth();
  const { mutate, isLoading } = useUploadScreenshot();
  const input = useRef<HTMLInputElement>(null);
  const { replace } = useRouter();

  if (!d || error) {
    return null;
  }

  return (
    <div className={s.workspace}>
      <p className={s.header}>Upload Screenshot</p>
      <form
        className={s.form}
        onSubmit={(ev) => {
          ev.preventDefault();
          // @ts-expect-error
          const fd = new FormData(ev.target);
          console.log(fd);
          mutate(
            { f: fd, name: "" },
            {
              onSuccess: (d) => {
                replace(`/projects/${d.id}`);
              },
            }
          );
        }}
      >
        <input
          name="name"
          defaultValue="Название"
          className={s.input}
          ref={input}
        />

        <input
          className={s.file}
          type="file"
          name="file"
          onChange={(ev) => {
            if (input.current) {
              input.current.value = ev.target.files?.[0].name ?? "";
            }
          }}
        />

        <button type="submit" className={s.button} disabled={isLoading}>
          {isLoading ? "Uploading..." : "Upload"}
        </button>
      </form>
    </div>
  );
}

export default Home;
