import Link from "next/link";
import s from "./Menu.module.css";
import { useParams, useRouter } from "next/navigation";
import { useProfile } from "@/services/user";
import { useDeleteProject, useProjects } from "@/services/projects";
import clsx from "clsx";

const Menu: React.FC = () => {
  const { data: profile } = useProfile();
  const { data: projects = [] } = useProjects();
  const { id } = useParams();
  const { replace } = useRouter();
  const { mutate: del } = useDeleteProject();

  return (
    <div className={s.contactList}>
      <div className={s.logotype}>
        <span>Button Detection</span>
      </div>

      <div className={s.content}>
        <p className={s.contentTitle}>History</p>
        {projects.length === 0 && (
          <p className={s.contentMessage}>No projects are found</p>
        )}
        {projects.map((project) => (
          <Link key={project.id} href={`/projects/${project.id}`}>
            <button
              type="button"
              className={`${[
                s.contact,
                id === project.id.toString() && s.contact_active,
              ].join(" ")}`}
            >
              <div className={s.details}>
                <div className={s.name}>
                  {project.element.name ?? `Button #${project.id}`}
                </div>
              </div>

              <span
                className={s.contactDelete}
                onClick={(ev) => {
                  ev.preventDefault();
                  del(project.id, {
                    onSuccess: () => {
                      console.log({
                        id,
                        pid: project.id.toString(),
                        eq: id !== project.id.toString(),
                      });

                      if (id !== project.id.toString()) return;

                      const v = projects.find(({ id }) => project.id !== id);

                      if (id && v) {
                        replace(`/projects/${v.id}`);
                      } else {
                        replace(`/projects`);
                      }
                    },
                  });
                }}
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="32"
                  height="32"
                  viewBox="0 0 32 32"
                  fill="none"
                >
                  <path
                    d="M18.7404 13L18.3942 22M13.6058 22L13.2596 13M23.2276 9.79057C23.5696 9.84221 23.9104 9.89747 24.25 9.95629M23.2276 9.79057L22.1598 23.6726C22.0696 24.8448 21.0921 25.75 19.9164 25.75H12.0836C10.9079 25.75 9.93037 24.8448 9.8402 23.6726L8.77235 9.79057M23.2276 9.79057C22.0812 9.61744 20.9215 9.48485 19.75 9.39432M7.75 9.95629C8.08957 9.89747 8.43037 9.84221 8.77235 9.79057M8.77235 9.79057C9.91878 9.61744 11.0785 9.48485 12.25 9.39432M19.75 9.39432V8.47819C19.75 7.29882 18.8393 6.31423 17.6606 6.27652C17.1092 6.25889 16.5556 6.25 16 6.25C15.4444 6.25 14.8908 6.25889 14.3394 6.27652C13.1607 6.31423 12.25 7.29882 12.25 8.47819V9.39432M19.75 9.39432C18.5126 9.2987 17.262 9.25 16 9.25C14.738 9.25 13.4874 9.2987 12.25 9.39432"
                    stroke="rgb(var(--c))"
                    strokeWidth="1.4"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  />
                </svg>
              </span>
            </button>
          </Link>
        ))}
      </div>

      <div className={clsx(s.createNewChatBlock, "flex", "gap-4")}>
        <Link href="/projects" className={s.createNewChatButton}>
          Upload
        </Link>

        <Link href="/projects/showroom" className={s.createNewChatButton}>
          Showroom
        </Link>
      </div>

      <div className={s.profile}>
        <span className={s.profileAvatar}>
          {profile?.username?.[0]?.toUpperCase() ?? "?"}
          {profile?.email?.[0]?.toUpperCase() ?? "?"}
        </span>
        <div className={s.profileUserInfo}>
          <p className={s.profileUserName}>{profile?.username}</p>
          <p className={s.profileUserEmail}>{profile?.email}</p>
        </div>

        <button className={s.profileLogout} type="button">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="32"
            height="32"
            viewBox="0 0 32 32"
            fill="none"
          >
            <path
              fillRule="evenodd"
              clipRule="evenodd"
              d="M9 11.4286C9 9.53503 10.535 8 12.4286 8H16.6191C18.3022 8 19.6667 9.36447 19.6667 11.0476V11.8095C19.6667 12.2303 19.3256 12.5714 18.9048 12.5714C18.484 12.5714 18.1429 12.2303 18.1429 11.8095V11.0476C18.1429 10.206 17.4607 9.52381 16.6191 9.52381H12.4286C11.3766 9.52381 10.5238 10.3766 10.5238 11.4286V19.8095C10.5238 20.8615 11.3766 21.7143 12.4286 21.7143H16.6191C17.4607 21.7143 18.1429 21.0321 18.1429 20.1905V19.4286C18.1429 19.0078 18.484 18.6667 18.9048 18.6667C19.3256 18.6667 19.6667 19.0078 19.6667 19.4286V20.1905C19.6667 21.8736 18.3022 23.2381 16.6191 23.2381H12.4286C10.535 23.2381 9 21.7031 9 19.8095V11.4286ZM21.4137 12.7946C21.7112 12.497 22.1936 12.497 22.4911 12.7946L24.7769 15.0803C25.0744 15.3778 25.0744 15.8603 24.7769 16.1578L22.4911 18.4435C22.1936 18.741 21.7112 18.741 21.4137 18.4435C21.1161 18.146 21.1161 17.6636 21.4137 17.366L22.3987 16.381H15.8572C15.4364 16.381 15.0952 16.0399 15.0952 15.6191C15.0952 15.1983 15.4364 14.8572 15.8572 14.8572H22.3987L21.4137 13.8721C21.1161 13.5745 21.1161 13.0921 21.4137 12.7946Z"
              fill="rgb(var(--c)"
            />
          </svg>
        </button>
      </div>
    </div>
  );
};

export default Menu;
