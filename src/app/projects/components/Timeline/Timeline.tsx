import { useMemo, type FC } from "react";
import {
  Description,
  Field,
  Input,
  Label,
  Listbox,
  ListboxButton,
  ListboxOption,
  ListboxOptions,
  Transition,
} from "@headlessui/react";
import clsx from "clsx";

import s from "./Timeline.module.css";
import { Controller, useFormContext } from "react-hook-form";
import { Colors, Tokens } from "@/config/const";
import { TokenDto, TokenEntity } from "@/services/projects";
import { useProject } from "@/services/projects";
import { CodeBlock } from "react-code-block";
import { themes } from "prism-react-renderer";

const people = [
  { id: Colors.BLUE, name: Colors.BLUE },
  { id: Colors.GOLD, name: Colors.GOLD },
  { id: Colors.ORANGE, name: Colors.ORANGE },
  { id: Colors.PINK, name: Colors.PINK },
  { id: Colors.PURPLE, name: Colors.PURPLE },
];

//// - текст;
// - жирность этого текста (лайт, нормал, болд);
// - фемили: (моно, с засечками, без засечек);
//// - цвет фона;
// - размеры;
// - бордер-радиусы (круглая и квадратная);

const showableTokens = [
  "color",
  // "font-family",
  // "font-style",
  // "text-align",
  "text-transform",
  "background-color",
  "border-color",
  "border-style",
];

const generateCss = (tokens: TokenDto) => {
  const rows = Object.keys(tokens)
    .filter((v) => showableTokens.includes(v))
    .map((v) => `  ${v}: ${tokens[v as keyof TokenDto]};`);

  return [".button {", ...rows, "}"].join("\n");
};

const Timeline: FC<{ projectId?: string }> = ({ projectId }) => {
  // const { data = [] } = useFrames();
  const { register, watch, control } = useFormContext();
  const { data: project } = useProject(projectId!);
  const [color, bgc] = watch(["color", "bgc"]);

  const css = useMemo(() => {
    if (!project) return ".no-tokens-for-you {}";
    const { isNoDetection, ...rest } = project.element.css;
    return generateCss({
      ...rest,
      color: (color && color.id) ?? rest.color,
      "background-color": (bgc && bgc.id) ?? rest["background-color"],
    });
  }, [project?.element.css, color, bgc]);

  return (
    <div className={s.contactList}>
      <div className={s.logotype}>Design Tokens</div>

      <div className={s.content}>
        <Field className="mb-6">
          <Label className="text-sm/6 font-medium text-black">
            Original Image
          </Label>

          <img
            src={`data:image/jpeg;base64,${project?.element.picture}`}
            className="rounded-lg my-2 border border-black/10"
          />

          <Description className="text-sm/6 text-black/50">
            Preserved for you to behold magic
          </Description>
        </Field>

        {!project?.element.isNoDetection && (
          <>
            <Field className="mb-6 overflow-hidden">
              <Label className="text-sm/6 font-medium text-black">
                Design Tokens
              </Label>
              <CodeBlock code={css} language="css" theme={themes.github}>
                <CodeBlock.Code className="bg-white mt-3 p-6 rounded-xl overflow-x-scroll">
                  <div className="table-row">
                    <CodeBlock.LineContent className="table-cell">
                      <CodeBlock.Token />
                    </CodeBlock.LineContent>
                  </div>
                </CodeBlock.Code>
              </CodeBlock>
            </Field>

            <Field className="mb-6">
              <Label className="text-sm/6 font-medium text-black">
                Caption
              </Label>
              <Description className="text-sm/6 text-black/50">
                Extracted text caption could be modified
              </Description>
              <Input
                {...register(Tokens.TEXT)}
                className={clsx(
                  "mt-3 block w-full rounded-lg border-none bg-black/5 py-1.5 px-3 text-sm/6 text-black",
                  "focus:outline-none data-[focus]:outline-2 data-[focus]:-outline-offset-2 data-[focus]:outline-black/25"
                )}
              />
            </Field>

            <Field className="mb-6">
              <Label className="text-sm/6 font-medium text-black">
                Background Color
              </Label>
              <Description className="text-sm/6 text-black/50">
                Approximated by our finest algorithm
              </Description>

              <Controller
                control={control}
                name={Tokens.COLOR}
                render={({ field: { onChange, onBlur, value, ref } }) =>
                  value && (
                    <Listbox onChange={onChange} value={value}>
                      <ListboxButton
                        onBlur={onBlur}
                        className={clsx(
                          "mt-3 relative block w-full rounded-lg bg-black/5 py-1.5 px-3 text-left text-sm/6 text-black",
                          "focus:outline-none data-[focus]:outline-2 data-[focus]:-outline-offset-2 data-[focus]:outline-black/25 flex justify-between w-full"
                        )}
                      >
                        <span>{value.name}</span>

                        <span
                          className="w-[24px] h-[24px] rounded block"
                          style={{ background: value.id }}
                        />
                      </ListboxButton>

                      <Transition
                        leave="transition ease-in duration-100"
                        leaveFrom="opacity-100"
                        leaveTo="opacity-0"
                      >
                        <ListboxOptions
                          anchor="bottom"
                          className="w-[var(--button-width)] rounded-xl border border-black/5 bg-white p-1 [--anchor-gap:var(--spacing-1)] focus:outline-none"
                        >
                          {people.map((person) => (
                            <ListboxOption
                              key={person.name}
                              value={person}
                              className="group flex cursor-default items-center gap-2 rounded-lg py-1.5 px-3 select-none data-[focus]:bg-black/5"
                            >
                              <div className="text-sm/6 text-black flex justify-between w-full">
                                <span>{person.name}</span>

                                <span
                                  className="w-[24px] h-[24px] rounded block"
                                  style={{ background: person.id }}
                                />
                              </div>
                            </ListboxOption>
                          ))}
                        </ListboxOptions>
                      </Transition>
                    </Listbox>
                  )
                }
              />
            </Field>

            <Field>
              <Label className="text-sm/6 font-medium text-black">
                Text Color
              </Label>
              <Description className="text-sm/6 text-black/50">
                Approximated by our finest algorithm
              </Description>

              <Controller
                control={control}
                name="bgc"
                render={({ field: { onChange, onBlur, value, ref } }) =>
                  value && (
                    <Listbox onChange={onChange} value={value}>
                      <ListboxButton
                        onBlur={onBlur}
                        className={clsx(
                          "mt-3 relative block w-full rounded-lg bg-black/5 py-1.5 px-3 text-left text-sm/6 text-black",
                          "focus:outline-none data-[focus]:outline-2 data-[focus]:-outline-offset-2 data-[focus]:outline-black/25 flex justify-between w-full"
                        )}
                      >
                        <span>{value.name}</span>

                        <span
                          className="w-[24px] h-[24px] rounded block"
                          style={{ background: value.id }}
                        />
                      </ListboxButton>

                      <Transition
                        leave="transition ease-in duration-100"
                        leaveFrom="opacity-100"
                        leaveTo="opacity-0"
                      >
                        <ListboxOptions
                          anchor="bottom"
                          className="w-[var(--button-width)] rounded-xl border border-black/5 bg-white p-1 [--anchor-gap:var(--spacing-1)] focus:outline-none"
                        >
                          {people.map((person) => (
                            <ListboxOption
                              key={person.name}
                              value={person}
                              className="group flex cursor-default items-center gap-2 rounded-lg py-1.5 px-3 select-none data-[focus]:bg-black/5"
                            >
                              <div className="text-sm/6 text-black flex justify-between w-full">
                                <span>{person.name}</span>

                                <span
                                  className="w-[24px] h-[24px] rounded block"
                                  style={{ background: person.id }}
                                />
                              </div>
                            </ListboxOption>
                          ))}
                        </ListboxOptions>
                      </Transition>
                    </Listbox>
                  )
                }
              />
            </Field>
          </>
        )}
      </div>
    </div>
  );
};

export default Timeline;
