"use client";

import { Inter } from "next/font/google";
import { QueryClientProvider, QueryClient } from "react-query";
import "./globals.css";

const inter = Inter({ subsets: ["latin"] });

const client = new QueryClient();

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <QueryClientProvider client={client}>
      <html lang="en">
        <body className={inter.className}>{children}</body>
      </html>
    </QueryClientProvider>
  );
}
